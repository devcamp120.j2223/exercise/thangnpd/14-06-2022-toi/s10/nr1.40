//Import usermodel
const userModel = require('../models/userModel');

// Khai báo thư viện mongoose để tạo _id
const mongoose = require("mongoose");

const createUser = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;

  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.username) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "username is required"
    })
  }

  if (!bodyRequest.firstname) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "firstname is required"
    })
  }

  if (!bodyRequest.lastname) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "lastname is required"
    })
  }

  // B3: Thao tác với cơ sở dữ liệu
  let createUser = {
    _id: mongoose.Types.ObjectId(),
    username: bodyRequest.username,
    firstname: bodyRequest.firstname,
    lastname: bodyRequest.lastname
  }

  userModel.create(createUser, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(201).json({
        status: "Success: User created",
        data: data
      })
    }
  })
}

const getAllUser = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  userModel.find((error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Get all users success",
        data: data
      })
    }
  })
}

const getUserById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let userId = request.params.userId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "userId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  userModel.findById(userId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Get user by ID success",
        data: data
      })
    }
  })
}

const updateUserById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let userId = request.params.userId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "User ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let userUpdate = {
    firstname: bodyRequest.firstname,
    lastname: bodyRequest.lastname
  }

  userModel.findByIdAndUpdate(userId, userUpdate, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Update user by ID success",
        data: data
      })
    }
  })
}

const deleteUserById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let userId = request.params.userId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(userId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "Course ID is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  userModel.findByIdAndDelete(userId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(204).json({
        status: "Success: Delete user success"
      })
    }
  })
}

module.exports = {
  createUser,
  getAllUser,
  getUserById,
  updateUserById,
  deleteUserById
}